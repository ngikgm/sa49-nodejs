'use strict';

const { Firestore } = require('@google-cloud/firestore');
const { Storage } = require('@google-cloud/storage');

const firestore = new Firestore();
console.log(firestore);

// Creates a client
const storage = new Storage();

async function test(){
    const document = firestore.collection('post').doc('config');
    
    await document.set({
        width: 640,
        height: 480
    });
    console.log("Insert data to firestore");

    const document2 = firestore.collection('post').doc();
    
    await document2.set({
        title: 'Kenneth Phang',
        age: 40,
        place: 'Singapore'
    });
    console.log("Insert data to firestore");
    await document2.update({
        age: 55
    });
    console.log("update data to firestore");

    let doc = await document2.get();
    console.log(doc);
    console.log("retrieve from firestore");

    await document2.delete();
    console.log("delete record on firestore");
}



async function uploadFile() {
  // Uploads a local file to the bucket
  await storage.bucket('test').upload('./teddy-small-size-500x500.jpg', {
    gzip: true,
    metadata: {
      cacheControl: 'public, max-age=31536000',
    },
  });

  console.log(`${filename} uploaded to ${bucketName}.`);
}

test();
uploadFile();
