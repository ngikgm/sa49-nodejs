console.log("Test !");
const mkPower = function(exp){
    const power = function(base){
        let ans = 1;
        for(let i=0; i < exp; i++){
            ans *= base
        }
        return (ans);
    }
    return (power);
}

const square = mkPower(2);
console.info(square);
console.info('6^2 = ', square(6));

console.time('100-elements');
for (let i = 0; i < 100000; i++) {}
console.timeEnd('100-elements');
test=true;
console.assert(test, "hello!");

test2 = null;
console.log(test2);
// console.log(test3);

// function test(t) {
//     if (t === undefined) {
//        return 'Undefined value!';
//     }
//     return t;
//   }
  
// let x;
  
//console.log(test(x));

var xarr = [1,3,4,6,7,9];
xarr.push(2);
console.log(xarr);
xarr.pop();
console.log(xarr);

xarr.unshift(1);
console.log(xarr);

xarr.shift();
console.log(xarr);

var newArr = xarr.join('|'); //after string
console.log(typeof(newArr));
console.log(newArr);
newArr = newArr.split('|');
newArr.push(99);
console.log(newArr);

function initDB(){
    console.log("db init ... done");
}

function initMQ(){
    console.log("mq init done ...");
}

( ()=> {
    console.log("print !!!");
    initDB();
    initMQ();
})();


setTimeout(initDB, 3000);


function testError(){
    throw new Error("ERROR");
}

try {
    testError();
}catch(e){
    console.log(e);
}

function Shape(){

}

Shape.prototype.X = 0;
Shape.prototype.Y = 0;


Shape.prototype.move = function(x, y){
    this.X = x;
    this.Y = y;
}

Shape.prototype.distance_from_origin = function () {
    return Math.sqrt(this.X*this.X + this.Y*this.Y);
}

var s = new Shape()
s.move(10, 30);
console.log(">>> " + s.distance_from_origin());

var x = 3;
//x = true;
//x = 'Test';
console.log(typeof(x));
if(x == '3') {
    console.log("double is 3");
}

if( x === 3){
    console.log("triple is 3");
}