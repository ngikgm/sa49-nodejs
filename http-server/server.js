require('dotenv').config();
const express = require('express');

const app = express();

const APP_PORT = process.env.PORT;

app.get('/hello-world', (req,res)=>{
    res.send('Hello World');
});

console.log(__dirname);
app.use(express.static(__dirname + '/public'));

app.use((req,res)=>{
    res.status(404).type('text/html')
        .send("Not found !");
});

app.listen(APP_PORT, ()=>{
    console.log(`Application server started on ${APP_PORT}`);
})